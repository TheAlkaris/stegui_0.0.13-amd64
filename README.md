# stegui_0.0.1-13-amd64
SteGUI Frontend for steghide

Steganography tool for encrypting and hiding files within another file -> See [Wiki](https://en.wikipedia.org/wiki/Steganography).


-- Converted RPM package to DEB using Alien 8.95

install with;

    dpkg -i stegui_0.0.1-13_amd64.deb

### Dependenceies

      * steghide
      * segsnow
      * libc6
      * libgcc1
      * libjpeg8
      * libcrypt4
      * libmhash2
      * zlib1g
      
      
**DISCLAIMER:** You are downloading and using this at your own risk. I shall not be held liable or responsible for losss of data or destruction of your computer. Use at your own risk.
If you have issues you should take it up with the maintainer of the RPM package - http://stegui.sourceforge.net I am merely sharing a converted RPM package to DEB so it may be used and installed on Debian based systems. It should run fine once you have the necessary dependencies installed.
